@extends('demo.master')

@section('title')

    Halaman Cast {{$cast->nama}}

@endsection
@section('subtitle')

    Detail Cast

@endsection

@section('content')

<h3> {{$cast->nama}} </h3>
<p> Umur {{$cast->umur}} tahun</p>
<p> {{$cast->bio}}</p>

@endsection