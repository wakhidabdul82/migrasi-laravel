@extends('demo.master')

@section('title')

    Halaman Cast

@endsection
@section('subtitle')

    Post Cast

@endsection

@section('content')
<a href="/cast/create" class="btn btn-success mb-3">Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$items)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$items->nama}}</td>
                <td>{{$items->umur}}</td>
                <td>{{$items->bio}}</td>
                <td>
                    
                    <form action="/cast/{{$items->id}}" method="POST">
                    <a href="/cast/{{$items->id}}" class="btn btn-info btn-sm ">Detail</a>
                    <a href="/cast/{{$items->id}}/edit" class="btn btn-warning btn-sm ">Edit</a>
                    @method('DELETE')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Kosong</td>
            </tr>
        @endforelse
      
    </tbody>
</table>

@endsection